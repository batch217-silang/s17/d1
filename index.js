// console.log('hello world');
// FUNCTIONS - codes to perform
// syntax:
	/*function functionName() {
		code block (statement)
	}*/

// function keywords - used to define a js function
// functionName - we set name so that we an use it for later
// function block ({}) - this is where code to be executed
// () - parameter

// 1

function printName() {
	console.log("My name is John");
}
printName();    /*invocation - calling a function that needs to be executed*/
function declaredFunction(){
	console.log("Hello World from declaredFunction");
}
declaredFunction();

// 2. Function expression-stored in a variable

let variableFunction = function(){
	console.log('Hello Again!');
}
variableFunction();
let funcExpression = function funcName(){
	console.log("Hello from the other side.")
}
funcExpression();

// funcExpression = function (){
// 	console.log("Updated funcExpression")
// }
// funcExpression();

// reassigning declaredFunction() value
declaredFunction = function(){
	console.log("Updated declaredFunction");
}
declaredFunction();
declaredFunction();
declaredFunction();


// re-assigning function declared with const
const constantFunc = function(){
	console.log("Initialized with const!")
}
constantFunc();

// // re-assignment of a const function - No, const can't be reassigned
// constantFunc = function(){
// 	console.log("Can we reassign it?")
// }
// constantFunc();

// FUNCTION SCOPING - 
/*
1. local/block scope { let name = 'john'}
2. global scope 
3. function scope
	JS has function scope: each function creates a new scope.
*/

// 1. local/block scope { let name = 'john'}, accessible only inside the {}
{
	let localVar = 'Armando Perez';
	console.log(localVar);
}

// 2. global scope - accessible only after declaration
let gobalScope = "Mr. Worldwide";
console.log(gobalScope);

// Function scoping
function showNames(){
	// Function scope variable:
	const functionConst = "John";
	let functionLet = "Jane";
	console.log(functionConst);
	console.log(functionLet);
}
showNames();

// NESTED FUNCTION - creating function inside a function inside another function
function myNewFunction(){
	let name = "Jane";
	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();
// nestedFunction(); --error due to location: outside the nest


// Function and Global scoped Variable
// Golbal scoped variable
let globalName = "Alexandro";
function myNewFunction2(){
	let nameInside = "Renz";
	// can be accessed inside
	console.log(globalName);
}
myNewFunction2();
// console.log(nameInside);

// USING ALERT()
alert("Hello World!");    /*executed immediately*/
function showAlert(){
	alert("Hello User!");

}
showAlert();
console.log("I will only log in the console when alert is dismissed.");

// USING PROMPT
// let samplePrompt = prompt("Enter your Name.");
// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

// prompt() can be used to gather user input
// prompt() it can be run immediately

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");
	console.log("hello, " + firstName + lastName + "!");
	console.log("Welcome to my page!");
};
printWelcomeMessage();

// FUNCTION NAMING CONVENTION-definitive of the tast, contains a VERB

function getCourse() {
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}
getCourse();

// Avoid generic names to avoid confusion 
function get(){
	let name = "Jamie";
	console.log(name);
}
get();

// Avoid pointless and inappropriate function names.

function foo(){
	console.log(25%5);
}
foo();

// name your function in small caps followed by camelCase when naming variables and functions


// camelCase ------ myNameIsCely
// snake_case ------my_name_is_cely
// kebab ------- my-name-is-cely


function displayCarInfo() {
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1, 500, 000");
}

displayCarInfo();